<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 20/05/2018
 * Time: 10:56
 */

namespace App;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Dispatcher
{
    /**
     * @var array
     */
    private $_middlewares = [];

    /**
     * @var int
     */
    private $_index = 0;

    /**
     * @param callable|Object $middleware
     */
    public function pipe($middleware)
    {
        $this->_middlewares[] = $middleware;
    }

    /**
     * @param ServerRequestInterface $request
     * @param Response $response
     */
    public function handle(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface
    {
        $middleware = $this->getMiddleware();
        $this->_index++;
        if (is_null($middleware)) {
            return $response;
        }
        return $middleware($request, $response, [$this, 'handle']);
    }

    /**
     * @param int $count
     */
    public function getMiddleware()
    {
        if (isset($this->_middlewares[$this->_index])) {
            return $this->_middlewares[$this->_index];
        }
        return null;
    }
}
