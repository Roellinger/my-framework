<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 21/05/2018
 * Time: 15:49
 */

namespace App\Renderer;

class TwigRenderer implements RendererInterface
{
    /**
     * @var
     */
    private $twig;

    /**
     * @var
     */
    private $loader;

    /**
     * TwigRenderer constructor.
     * @param String $path
     */
    public function __construct(String $path)
    {
        $this->loader = new \Twig_Loader_Filesystem([getcwd().$path]);
        $this->loader->addPath(getcwd().$path, "App");
        //$this->loader->addPath(dirname(__DIR__).'\..\Src\Blog\views', "Blog");
        $this->twig = new \Twig_Environment($this->loader, []);
    }

    /**
     * @param String $path
     * @param String $namespace
     */
    public function addPath(String $path, String $namespace)
    {
        $this->loader->addPath($path, $namespace);
    }

    /**
     * @param String $view
     * @param null|array $params
     * @return string
     */
    public function render(String $view, $params=null)
    {
        return $this->twig->render($view, $params);
    }
}
