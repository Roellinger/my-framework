<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 21/05/2018
 * Time: 15:51
 */

namespace App\Renderer;

interface RendererInterface
{
    /**
     * @param String $path
     * @param String $namespace
     * @return mixed
     */
    public function addPath(String $path, String $namespace);

    /**
     * @param String $view
     * @return mixed
     */
    public function render(String $view);
}
