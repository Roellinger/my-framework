<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 20/05/2018
 * Time: 21:55
 */

namespace App\Route;

use App\Renderer\RendererInterface;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Src\Blog\Controller\DemoController;

class Router implements RouteInterface
{
    /**
     * @var array
     */
    private $_routes = [];

    /**
     * @param string $path
     * @param callable|string $callable
     * @param array|null $params
     * @return Router
     */
    public function get(string $path, $callable, array $params=null) : Router
    {
        $this->call($path, $callable, 'GET', $params);
        return $this;
    }

    /**
     * @param string $path
     * @param callable|string $callable
     * @param array|null $params
     * @return Router
     */
    public function post(string $path, $callable, array $params=null) : Router
    {
        $this->call($path, $callable, 'POST', $params);
        return $this;
    }

    /**
     * @param string $path
     * @param $callable
     * @param string $method
     * @param array|null $params
     * @return $this
     */
    public function call(string $path, $callable, string $method, array $params=null)
    {
        $route = new Route();
        $route->setPath($path);
        if (isset($params['name'])) {
            $route->setName($params['name']);
        }
        $route->setCallable($callable);
        $this->_routes[$method][] = $route;
        return $this;
    }

    /**
     * @param ServerRequestInterface $request
     * @return mixed
     */
    public function match(ServerRequestInterface $request)
    {
        $uri = $request->getUri();
        $this->_path = $uri->getPath();
        if (isset($this->_routes[$request->getMethod()])) {
            foreach ($this->_routes[$request->getMethod()] as $route) {
                if ($this->matches($route)) {
                    return $route;
                }
            }
        }
        return null;
    }

    /**
     * @param Route $route
     * @return bool
     */
    private function matches(Route $route) : bool
    {
        $attributes = [];
        $uri = $route->getPath();
        preg_match_all("/:([\w]+)/", $uri, $mat);
        $path = preg_replace("/:([\w]+)/", "([^/]+)", $uri);
        $regex = "#^$path$#i";
        if (!preg_match($regex, $this->_path, $matches)) {
            return false;
        }
        array_shift($matches);
        $combine = array_combine($mat[1], $matches);
        $route->setParams($combine);
        return true;
    }

    /**
     * @param String $path
     * @param array $params
     * @return mixed|String
     */
    public function generateUri(String $path, array $params=[])
    {
        $get = $this->_routes['GET'] ?? [];
        foreach ($get as $route) {
            if ($route->getName() == $path) {
                $path = $route->getPath();
                foreach ($params as $k => $param) {
                    $path = preg_replace("/:$k/", $param, $route->getPath());
                }
                return $path;
            }
        }
        return null;
    }
}
