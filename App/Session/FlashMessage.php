<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 26/05/2018
 * Time: 12:03
 */

namespace App\Session;

class FlashMessage
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * FlashMessage constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @param String $key
     * @param String $message
     * @return bool
     */
    public function set(String $key, String $message) : bool
    {
        return $this->session->set($key, $message);
    }

    /**
     * @param String $key
     */
    public function get(String $key)
    {
        $message = $this->session->get($key);
        if (!is_null($message)) {
            $this->session->remove($key);
        }
        return $message;
    }

    /**
     * @param String $key
     * @return bool
     */
    public function has(String $key) : bool
    {
        if (is_null($this->session->get($key))) {
            return false;
        }
        return true;
    }
}
