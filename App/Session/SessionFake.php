<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 26/05/2018
 * Time: 11:31
 */

namespace App\Session;

use PHPUnit\Runner\Exception;

class SessionFake implements SessionInterface
{
    public $session = [];

    public function is_start()
    {
        if (session_status() == PHP_SESSION_NONE) {
            return false;
        }
        return true;
    }

    public function get($key)
    {
        if (!isset($this->session[$key])) {
            return null;
        }
        return $this->session[$key];
    }

    public function set($key, $value)
    {
        $this->session[$key] = $value;
        return $this;
    }

    public function remove($key)
    {
        if (!isset($this->session[$key])) {
            return false;
        }
        unset($this->session[$key]);
        return true;
    }
}
