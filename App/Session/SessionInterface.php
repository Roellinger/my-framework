<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 26/05/2018
 * Time: 11:28
 */

namespace App\Session;

/**
 * Interface SessionInterface
 * @package App\Session
 */
interface SessionInterface
{
    /**
     * @param $key
     * @return mixed
     */
    public function get($key);

    /**
     * @param $key
     * @param $value
     * @return mixed
     */
    public function set($key, $value);

    /**
     * @param $key
     * @return mixed
     */
    public function remove($key);
}
