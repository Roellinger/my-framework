<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 26/05/2018
 * Time: 11:29
 */

namespace App\Session;

class Session implements SessionInterface
{
    public function ensureStarted()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function get($key)
    {
        $this->ensureStarted();
        if (!isset($_SESSION[$key])) {
            return null;
        }
        return $_SESSION[$key];
    }

    public function set($key, $value)
    {
        $this->ensureStarted();
        $_SESSION[$key] = $value;
        return true;
    }

    public function remove($key)
    {
        $this->ensureStarted();
        if (!isset($_SESSION[$key])) {
            return false;
        }
        unset($_SESSION[$key]);
        return true;
    }
}
