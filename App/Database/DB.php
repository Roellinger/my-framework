<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 28/05/2018
 * Time: 16:55
 */

namespace App\Database;

class DB
{
    /**
     * @var string
     */
    private $base = "mysql";

    /**
     * @var string
     */
    private $db_name = "my_framework";
    /**
     * @var string
     */
    private $user = "root";
    /**
     * @var string
     */
    private $password = "root";
    /**
     * @var string
     */
    private $host = "127.0.0.1";

    /**
     * @var string
     */
    private $port = "8889";

    /**
     * @var null
     */
    private static $instance = null;

    private $pdoInstance;

    /**
     * DB constructor.
     * @param string $base
     * @param string $db_name
     * @param string $user
     * @param string $password
     * @param string $host
     * @param string $port
     */
    public function __construct()
    {
        $dsn = $this->base.":dbname=".$this->db_name.";host=".$this->host.";port=".$this->port;
        $this->pdoInstance = self::$instance = new \PDO($dsn, $this->user, $this->password);
    }

    /**
     * @return null|\PDO
     */
    public static function getInstance()
    {
        if(is_null(self::$instance)){
            self::$instance = new DB();
        }
        return self::$instance;
    }

    /**
     * @param $sql
     */
    public function query($sql){
        return $this->pdoInstance->query($sql);
    }

    /**
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param string $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }

    /**
     * @return string
     */
    public function getDbName()
    {
        return $this->db_name;
    }

    /**
     * @param string $db_name
     */
    public function setDbName($db_name)
    {
        $this->db_name = $db_name;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }
}