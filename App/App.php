<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 20/05/2018
 * Time: 09:56
 */

namespace App;

use App\Renderer\RendererInterface;
use App\Renderer\TwigRenderer;
use App\Route\Router;
use App\Route\RouteInterface;
use DI\Container;
use DI\ContainerBuilder;
use GuzzleHttp\Psr7\Response;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Src\Blog\BlogModule;
use function Http\Response\send;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class App
 * @package App
 */
class App
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var
     */
    private $modules;

    /**
     * App constructor.
     * @param ContainerInterface $container
     * @param array $modules
     */
    public function __construct(array $modules=[])
    {
        //$builder->useAutowiring(false);
        $builder = $this->buildContainer();
        $builder->enableCompilation(__DIR__ . '/tmp');
        $builder->writeProxiesToFile(true, __DIR__ . '/tmp/proxies');
        $builder->useAnnotations(true);
        $this->preloadDefinitions($builder, $modules);
        $this->container = $builder->build();
        foreach ($modules as $module) {
            $this->preloadPathTwigRenderer($module);
            $this->modules[] = $this->container->get($module);
        }
    }

    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param ContainerBuilder $builder
     * @param array $modules
     */
    public function preloadDefinitions(ContainerBuilder $builder, array $modules=[])
    {
        $builder->addDefinitions(dirname(__DIR__).'/config.php');
        foreach ($modules as $module) {
            if ($module::DEFINITIONS) {
                $builder->addDefinitions($module::DEFINITIONS);
            }
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @return bool|mixed
     */
    public function run(ServerRequestInterface $request)
    {
        $router = $this->container->get(RouteInterface::class);
        $route = $router->match($request);
        if (is_null($route)) {
            return new Response(404, [], '<h1>Not found</h1>');
        }
        $params = $route->getParams();
        foreach ($params as $k => $param) {
            $request = $request->withAttribute($k, $param);
        }

        if (!is_string($route->getCallable())) {
            $response = call_user_func_array($route->getCallable(), [$request]);
        } else {
            $split = explode('@', $route->getCallable());
            $namespace = $split[0]."Controller";
            $class = new $namespace;
            $response = call_user_func_array(array($class, $split[1]), array($request));
        }
        if (is_string($response)) {
            return new Response(200, [], $response);
        } elseif ($response instanceof ResponseInterface) {
            return $response;
        }
    }

    /**
     * create containerBuilder
     */
    private function buildContainer()
    {
        $builder = new ContainerBuilder();
        return $builder;
    }

    /**
     * @param $module
     */
    public function preloadPathTwigRenderer($module)
    {
        $twig = $this->container->get(TwigRenderer::class);
        $autoloader_reflector = new \ReflectionClass($module);
        $namespace = explode("\\", $autoloader_reflector->getNamespaceName());
        $class_file_name = $autoloader_reflector->getFileName();
        $path = dirname($class_file_name).$this->container->get('path.dir.views');
        if (file_exists($path)) {
            $twig->addPath($path, end($namespace));
        }
    }
}
