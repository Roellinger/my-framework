<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 23/05/2018
 * Time: 16:52
 */

namespace App;

use Psr\Container\ContainerInterface;

/**
 * Class Controller
 * @package App
 */
class Controller
{
    /**
     * Explicit definition of the entry to inject:
     *
     * @Inject("path.dir.views")
     */
    protected $container;

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }
    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
        return $this;
    }
}
