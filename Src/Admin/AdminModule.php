<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 21/05/2018
 * Time: 17:49
 */

namespace Src\Admin;

use App\Renderer\RendererInterface;
use App\Renderer\TwigRenderer;
use App\Route\Route;
use App\Route\RouteInterface;
use App\Route\Router;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class AdminModule
 * @package Src\Admin
 */
class AdminModule
{
    /**
     * CONST String
     */
    const DEFINITIONS = __DIR__."/config.php";

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * BlogModule constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $twig = $this->container->get(TwigRenderer::class);
    }
}