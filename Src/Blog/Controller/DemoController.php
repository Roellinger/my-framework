<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 21/05/2018
 * Time: 00:21
 */

namespace Src\Blog\Controller;

use App\Controller;
use App\Database\DB;
use App\Renderer\RendererInterface;
use App\Renderer\TwigRenderer;
use App\Route\RouteInterface;
use App\Route\Router;
use App\Session\FlashMessage;
use App\Session\Session;
use GuzzleHttp\Psr7\Response;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Src\Blog\BlogModule;

/**
 * Class DemoController
 * @package Src\Blog\Controller
 */

class DemoController extends Controller
{
    /**
     * @param ServerRequestInterface $request
     * @param RouteInterface $router
     * @return \GuzzleHttp\Psr7\MessageTrait|string|static
     */
    public function store(Request $request){
        $id = $request->getAttributes();
        var_dump($id);die;
        $flash = $container->get(FlashMessage::class);
        $router = $container->get(RouteInterface::class);
        $articles = DB::getInstance()->query("select * from articles where id = $id")->fetch();
        return $twig->render('@Blog/index.twig', ["articles" => $articles]);
    }

    /**
     *
     */
    public function view(Request $request, ContainerInterface $container){
        $flash = $container->get(FlashMessage::class);
        $session = $container->get(Session::class);
        $twig = $container->get(TwigRenderer::class);
        $id = $request->getAttribute('id');
        return $twig->render('@Blog/view.twig', ["id" => "$id", "session" => $session]);
    }

    /**
     * @param String $route
     */
    protected function redirect(String $route){
        return (new Response())
            ->withStatus(301)
            ->withHeader('Location', $route);
    }
}