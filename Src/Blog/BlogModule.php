<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 21/05/2018
 * Time: 17:49
 */

namespace Src\Blog;

use App\Renderer\RendererInterface;
use App\Renderer\TwigRenderer;
use App\Route\Route;
use App\Route\RouteInterface;
use App\Route\Router;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class BlogModule
 * @package Src\Blog
 */
class BlogModule
{
    /**
     * CONST String
     */
    const DEFINITIONS = __DIR__."/config.php";

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * BlogModule constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $twig = $this->container->get(TwigRenderer::class);
        $route = $this->container->get(RouteInterface::class);

        $route->get('/hello/', "Src\Blog\Controller\Demo@view", ["name" => "demo.article"]);
        $route->get('/article/:id', 'Src\Blog\Controller\Demo@store', ['name' => "demo.store"]);
    }
}