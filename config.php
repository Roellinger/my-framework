<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 21/05/2018
 * Time: 16:58
 */

/**
 * CONTAINER FILE DEFINITIONS
 *
 */

use function DI\create;
use function DI\get;

return [
    "mysql.user" => "root",
    "mysql.pass" => "root",
    "mysql.port" => "8889",
    "mysql.server" => "127.0.0.1",
    "path.dir.views" => "/resources/views",
    "main.dir.template" => "/App/views",
    \App\Session\Session::class => DI\create(\App\Session\Session::class),
    \App\Session\FlashMessage::class => create(\App\Session\FlashMessage::class)->constructor(get(\App\Session\Session::class)),
    \Psr\Http\Message\ServerRequestInterface::class => DI\factory([\GuzzleHttp\Psr7\ServerRequest::class, 'fromGlobals']),
    \Psr\Http\Message\ResponseInterface::class => DI\create(\GuzzleHttp\Psr7\Response::class),
    \App\Renderer\TwigRenderer::class => create(\App\Renderer\TwigRenderer::class)->constructor(get("main.dir.template")),
    \App\Route\RouteInterface::class => create(\App\Route\Router::class),
];