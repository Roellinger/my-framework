<?php

/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 20/05/2018
 * Time: 09:51
 */

require('./vendor/autoload.php');

use \Psr\Http\Message\ServerRequestInterface;

use function Http\Response\send;

/*$trainingSlash = function(ServerRequestInterface $request, \Psr\Http\Message\ResponseInterface $response, callable $next){
    $uri = $request->getUri();
    if(!empty($uri->getPath()) && $uri->getPath()[-1] === "/"){
        //$response = new \GuzzleHttp\Psr7\Response();
        return $response
            ->withHeader('Location', substr($uri->getPath(), 0, -1))
            ->withStatus(301);
    }
    return $next($request, $response);
};
$dispatcher = new \App\Dispatcher();
$dispatcher->pipe($trainingSlash);
send($dispatcher->handle($request, $container->get(\GuzzleHttp\Psr7\Response::class)));*/

$request = \GuzzleHttp\Psr7\ServerRequest::fromGlobals();

$app = new \App\App([
    \Src\Blog\BlogModule::class,
    \Src\Admin\AdminModule::class
]);

send($app->run($request));
