<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 23/05/2018
 * Time: 22:32
 */

namespace Test;

use App\App;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Src\Blog\BlogModule;
use Test\Modules\ResponseModule;

/**
 * Class AppTest
 * @package Test
 */
class AppTest extends TestCase
{
    public function testError404(){
        $app = new App();
        $request = new ServerRequest("GET", "/test/response");
        $response = $app->run($request);
        $this->assertContains("<h1>Not found</h1>", (string) $response->getBody());
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testResponse(){
        $app = new App([
            ResponseModule::class
        ]);
        $request = new ServerRequest("GET", "/test/response");
        $response = $app->run($request);
        $this->assertContains("response", (string) $response->getBody());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertInstanceOf(ResponseInterface::class, $response);
    }

    public function testRunEmpty(){
        $app = new App();
        $request = new ServerRequest("GET", "/test");
        $response = $app->run($request);
        $this->assertContains("<h1>Not found</h1>", (string) $response->getBody());
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testRunAppWithBadParam(){
        $app = new App([
            "test"
        ]);
    }
}