<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 24/05/2018
 * Time: 00:35
 */

namespace Test\Modules;

use App\Route\RouteInterface;
use Psr\Container\ContainerInterface;

class ResponseModule
{
    const DEFINITIONS = null;

    public function __construct(ContainerInterface $container){
        $this->container = $container;
        $route = $this->container->get(RouteInterface::class);
        $route->get('/test/response', function(){
            return "response";
        }, ['name' => "demo.index"]);
    }
}