<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 20/05/2018
 * Time: 22:05
 */

namespace Test\Route;


interface RouteInterface
{
    /**
     * @param String $path
     * @param callable|string $callable
     * @return mixed
     */
    public function get(String $path, $callable);

    /**
     * @param String $path
     * @param callable|string $callable
     * @return mixed
     */
    public function post(String $path, $callable);
}