<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 26/05/2018
 * Time: 11:32
 */

namespace Test;

use App\Session\SessionFake;
use PHPUnit\Exception;
use PHPUnit\Framework\TestCase;

class SessionTest extends TestCase
{
    public function testGetSessionNotExist()
    {
        $session = new SessionFake();
        $response = $session->get('user.id');

        $this->assertEquals(null, $response);
    }

    public function testSessionStart(){
        $session = new SessionFake();
        $response = $session->get('user.id');
        $this->expectException(\PHPUnit\Runner\Exception::class);
    }

    public function testGetSessionExist()
    {
        $session = new SessionFake();
        $session->set('user.id', 1);

        $response = $session->get('user.id');

        $this->assertEquals(1, $response);
    }

    public function testRemoveSucess()
    {
        $session = new SessionFake();
        $session->set('user.id', 1);
        $response = $session->remove('user.id');
        $this->assertSame(true, $response);
    }

    public function testRemoveFail()
    {
        $session = new SessionFake();
        $response = $session->remove('user.id');
        $this->assertSame(false, $response);
    }
}