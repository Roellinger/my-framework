<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 23/05/2018
 * Time: 21:30
 */

namespace Test;

use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Test\Route\Route;
use Test\Route\Router;

class RouterTest extends TestCase {

    /**
     *
     */
    public function testMatchRequest(){
        $router = new Router();
        $request = new ServerRequest("GET", "/test");
        $router->get('/test', function(){}, ['name' => 'test.request']);
        $response = $router->match($request);
        $this->assertInstanceOf(Route::class, $response);
        $this->assertEquals("/test", $response->getPath());
        $this->assertEquals("test.request", $response->getName());
    }

    /**
     *
     */
    public function testNotMatchRequest(){
        $router = new Router();
        $request = new ServerRequest("GET", "/tests");
        $router->get('/test', function(){});
        $response = $router->match($request);
        $this->assertEquals(null, $response);
    }

}