<?php
/**
 * Created by PhpStorm.
 * User: robinroellinger
 * Date: 26/05/2018
 * Time: 12:04
 */

namespace Test;

use App\Session\FlashMessage;
use App\Session\SessionFake;
use PHPUnit\Framework\TestCase;

class FlashMessageTest extends TestCase
{
    public function testIfFlashMessageIsDeleted(){
        $session = new SessionFake();
        $session->set("success", "value");
        $flashService = new FlashMessage($session);
        $response = $flashService->get("success");
        $response2 = $flashService->get("success");
        $this->assertEquals(null, $response2);
    }
}